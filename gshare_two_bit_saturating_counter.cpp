/*
	Austin Brennan, Robert Townsend
	CPSC 3300 Fall 2014
	Project 2, gShare program
*/

#include<stdio.h>
#include<stdlib.h>
#include<sstream>
#include<iostream>
#include<bitset>

using namespace std;

int main(int argc, char* argv[]) {

	// Declarations
	double rate;
	string hexStr, address;
	stringstream hexConvert;
	unsigned long hexInt;
	unsigned long totalCount = 0;
	unsigned long missCount = 0;
	int addressInt, shift;
	int bhsr = 0;
	int mask = 1;
	int shiftWithOne;	// OR'ed with bhsr if we need to push a 1
	int *pht;

	// Input validation
	if (argc != 2) {
		cout << "Usage: <project data list> | ./prog2 <# of bits>" << endl;
		return -1;
	}
	else {
		// Calculate mask value
		char* maskStr = argv[1];
		shift = atoi(maskStr);
		mask = (mask << shift) - 1;

		// Calculate shiftWithOne value
		shiftWithOne = 1;
		shiftWithOne = (shiftWithOne << (shift - 1));

		// Create PHT
		pht = new int[mask];
	}

	// Initialize PHT to zero
	int i = 0;
	for (i = 0; i < mask; i++) {
		pht[i] = 0;
	}

	// Process data
	while (cin >> hexStr >> address) {
		totalCount++;

		// Convert hexStr into a hex number
		hexInt = strtoul(hexStr.c_str(), NULL, 16);

		// Convert address into int
		if (address == "0") {
			addressInt = 0;
		}
		else if (address == "1") {
			addressInt = 1;
		}
		else {
			cout << "Error: Invalid branch value." << endl;
			return -1;
		}

		// Extract LSBs from hexInt
		int pc = hexInt & mask;

		// XOR PC and BHSR
		int index = pc ^ bhsr;


		//cout << totalCount << " - Before update:\tPC = " << (bitset<4>) pc << "\tBHSR = ";
		//cout << (bitset<4>) bhsr << "\tpht[" << index << "] = " << pht[index];
		//cout << "\tAddress = " << addressInt << endl;

		// Predict branch taken using PHT
		if (pht[index] > 1 && addressInt == 0) {
			// miss
			missCount++;

			// Branch was not taken, so push 0 onto BHSR
			bhsr = bhsr >> 1;

			// Decrement value in pht[index]
			if (pht[index] != 0) {
				pht[index]--;
			}
		}
		else if (pht[index] < 2 && addressInt == 1) {
			// miss
			missCount++;

			// Branch is taken, so push 1 onto BHSR
			bhsr = bhsr >> 1;
			bhsr = bhsr | shiftWithOne;

			// Increment value in pht[index]
			if (pht[index] != 3) {
				pht[index]++;
			}
		}
		else if (pht[index] > 1 && addressInt == 1) {
			// correctly predicted branch taken

			// Branch is taken, so push 1 onto BHSR
			bhsr = bhsr >> 1;
			bhsr = bhsr | shiftWithOne;

			// Increment value in pht[index]
			if (pht[index] != 3) {
				pht[index]++;
			}
		}
		else if (pht[index] < 2 && addressInt == 0) {
			// correctly predicted branch untaken

			// Branch was not taken, so push 0 onto BHSR
			bhsr = bhsr >> 1;

			// Decrement value in pht[index]
			if (pht[index] != 0) {
				pht[index]--;
			}
		}

		//cout << totalCount << " - After update:\tPC = " << (bitset<4>) pc << "\tBHSR = ";
		//cout << (bitset<4>) bhsr << "\tpht[" << index << "] = " << pht[index] << endl << endl;
	}


	// Calculate mispredict rate
	rate = missCount / (double) totalCount;
	rate *= 100;

	// Print out statistics
	cout << mask + 1 << " entries (" << shift << "-bit index): mispredicts:";
	cout << " = " << missCount << ", rate = " << rate << "%" << endl;

	// Free memory
	delete [] pht;

	return 0;
}
