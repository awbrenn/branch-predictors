/*
	Austin Brennan, Robert Townsend
	CPSC 3300 Fall 2014
	Project 2, SA scheme program
*/

#include<stdio.h>
#include<stdlib.h>
#include<sstream>
#include<iostream>
#include<bitset>

using namespace std;

int main(int argc, char* argv[]) {

	// Declarations
	double rate;
	int mask = 1;
	int bhsr = 0;
	int totalCount = 0;
	int missCount = 0;
	int shiftWithOne = 1 << 3;	// OR'ed with BHSR when pushing 1 to it
	int addressInt, hexInt, pc, shift;
	int table[512][17];		// 1st col is BHSR table, rest are PHTs
	string hexStr, address;

	// Initialize table to zero
	int i, j;
	for (i = 0; i < 512; i++) {
		for (j = 0; j < 17; j++) {
			table[i][j] = 0;
		}
	}

	// Input validation
	if (argc != 2) {
		cout << "Usage: <project data list> | ./prog3 <# of bits>" << endl;
		return -1;
	}
	else {
		// Calculate mask value
		char* maskStr = argv[1];
		shift = atoi(maskStr);
		mask = (mask << shift) - 1;
	}

	// Process data	
	while(cin >> hexStr >> address) {
		totalCount++;

		// Convert hexStr into a hex int
		hexInt = strtoul(hexStr.c_str(), NULL, 16);

		// Convert address into int
		if (address == "0") {
			addressInt = 0;
		}
		else if (address == "1") {
			addressInt = 1;
		}
		else {
			cout << "Error: Invalid branch value." << endl;
			return -1;
		}

		// Extract LSBs from hexInt. This is the row index
		pc = hexInt & mask;

		// Use BHSR value as column index
		int col = table[pc][0] + 1;

		// Miss cases
		if (table[pc][col] > 1 && addressInt == 0) {
			missCount++;

			// Update BHSR with 0
			bhsr = bhsr >> 1;

			// Update PHT entry
			if (table[pc][col] != 0) {
				table[pc][col]--;
			}
		}
		else if (table[pc][col] < 2 && addressInt == 1) {
			missCount++;

			// Update BHSR with 1
			bhsr = bhsr >> 1;
			bhsr = bhsr | shiftWithOne;

			// Update PHT entry
			if (table[pc][col] != 3) {
				table[pc][col]++;
			}
		}

		// Correct prediction cases
		else if (table[pc][col] > 1 && addressInt == 1) {
			// Update BHSR with 1
			bhsr = bhsr >> 1;
			bhsr = bhsr | shiftWithOne;

			// Update PHT entry
			if (table[pc][col] != 3) {
				table[pc][col]++;
			}
		}
		else if (table[pc][col] < 2 && addressInt == 0) {
			// Update BHSR with 0
			bhsr = bhsr >> 1;

			// Update PHT entry
			if (table[pc][col] != 0) {
				table[pc][col]--;
			}
		}
	}

	// Calculate mispredict rate
	rate = missCount / (double) totalCount;
	rate *= 100;

	// Print out statistics
	cout << mask + 1 << " entries (" << shift << "-bit index): mispredicts:";
	cout << " = " << missCount << ", rate = " << rate << "%" << endl;


	return 0;
}
