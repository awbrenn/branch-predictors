CC = g++
CCFLAGS = -Wall

all: prog1 prog2 prog3

prog1:
	$(CC) $(CCFLAGS) -o prog1 two_bit_saturating_counter.cpp

prog2:
	$(CC) $(CCFLAGS) -o prog2 gshare_two_bit_saturating_counter.cpp

prog3:
	$(CC) $(CCFLAGS) -o prog3 sa_scheme.cpp

clean:
	-rm *.o *.a *.out

job1:
	zcat server_trace_1_4M.txt.gz | ./prog1 4
	zcat server_trace_1_4M.txt.gz | ./prog1 5
	zcat server_trace_1_4M.txt.gz | ./prog1 6
	zcat server_trace_1_4M.txt.gz | ./prog1 7
	zcat server_trace_1_4M.txt.gz | ./prog1 8
	zcat server_trace_1_4M.txt.gz | ./prog1 9
	zcat server_trace_1_4M.txt.gz | ./prog1 10
	zcat server_trace_1_4M.txt.gz | ./prog1 11
	zcat server_trace_1_4M.txt.gz | ./prog1 12
	zcat server_trace_1_4M.txt.gz | ./prog1 13
	zcat server_trace_1_4M.txt.gz | ./prog1 14
	zcat server_trace_1_4M.txt.gz | ./prog1 15

job2:
	zcat server_trace_1_4M.txt.gz | ./prog2 4
	zcat server_trace_1_4M.txt.gz | ./prog2 5
	zcat server_trace_1_4M.txt.gz | ./prog2 6
	zcat server_trace_1_4M.txt.gz | ./prog2 7
	zcat server_trace_1_4M.txt.gz | ./prog2 8
	zcat server_trace_1_4M.txt.gz | ./prog2 9
	zcat server_trace_1_4M.txt.gz | ./prog2 10
	zcat server_trace_1_4M.txt.gz | ./prog2 11
	zcat server_trace_1_4M.txt.gz | ./prog2 12
	zcat server_trace_1_4M.txt.gz | ./prog2 13
	zcat server_trace_1_4M.txt.gz | ./prog2 14
	zcat server_trace_1_4M.txt.gz | ./prog2 15

job3:
	zcat server_trace_1_4M.txt.gz | ./prog3 9

test1:
	zcat server_trace_1_4M.txt.gz | ./prog1 2
	zcat server_trace_1_4M.txt.gz | ./prog1 3
	zcat server_trace_1_4M.txt.gz | ./prog1 16

test2:
	zcat server_trace_1_4M.txt.gz | ./prog2 2
	zcat server_trace_1_4M.txt.gz | ./prog2 3
	zcat server_trace_1_4M.txt.gz | ./prog2 16

test3:
	zcat server_trace_1_4M.txt.gz | ./prog3 2
	zcat server_trace_1_4M.txt.gz | ./prog3 3
	zcat server_trace_1_4M.txt.gz | ./prog3 16
