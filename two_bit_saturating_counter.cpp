/*
	Austin Brennan, Robert Townsend
	CPSC 3300 Fall 2014
	Project 2, 2-bit Saturating program
*/

#include<stdlib.h>
#include<stdio.h>
#include<sstream>
#include<iostream>

using namespace std;

int main(int argc, char* argv[]) {

	// Declarations
	double rate;
	int addressInt, shift;
	int mask = 1;
	int *bht;
	string hexStr, address;
	stringstream hexConvert;
	unsigned long hexInt;
	unsigned long totalCount = 0;
	unsigned long missCount = 0;

	// Input validation
	if (argc != 2) {
		cout << "Usage: <project data list> | ./prog1 <# of bits>" << endl;
		return -1;
	}
	else {
		// Calculate mask value, then create BHT
		char* maskStr = argv[1];
		shift = atoi(maskStr);
		mask = (mask << shift) - 1;
		bht = new int[mask];
	}

	// Initialize BHT to zero
	int i = 0;
	for (i = 0; i < mask; i++) {
		bht[i] = 0;
	}	

	// Process data
	while(cin >> hexStr >> address) {
		//cout << hexStr << " " << address << endl;
		totalCount++;
		
		// Convert hexStr into a hex number
		hexInt = strtoul(hexStr.c_str(), NULL, 16);

		// Convert address into int
		if (address == "0") {
			addressInt = 0;
		}
		else if (address == "1") {
			addressInt = 1;
		}
		else {
			cout << "Error: Invalid branch value." << endl;
			return -1;
		}

		// Extract LSB from hexInt
		int index = hexInt & mask;	

		// Determine if this is a miss
		if (bht[index] > 1 && addressInt == 0) {
			// miss
			missCount++;
		}
		else if (bht[index] < 2 && addressInt == 1) {
			// miss
			missCount++;
		}

		// Increment or decrement bht[index] based on addressInt
		if (addressInt == 0 && bht[index] != 0) {
			bht[index]--;
		}
		else if (addressInt == 1 && bht[index] != 3) {
			bht[index]++;
		}
	}

	// Calculate mispredict rate
	rate = missCount / (double) totalCount;
	rate *= 100;

	// Print out statistics
	cout << mask + 1 << " entries (" << shift << "-bit index): mispredicts:";
	cout << " = " << missCount << ", rate = " << rate << "%" << endl;

	// Free memory
	delete [] bht;

	return 0;
}
